The Kobben class (also known as Type 207) is a customized version of the German Type 205 submarine. Fifteen vessels of this class were built for use by the Royal Norwegian Navy in the 1960s. The class later saw service with Denmark and Poland. The boats have since been withdrawn from service in the Norwegian,Polish and Danish navies. 

Along with the rest of the Royal Norwegian Navy, the submarine fleet was to be modernized according to the Fleet plan of 1960. After the war, Norway needed a navy more suited for coastal operations rather than large, seagoing vessels. This made the choice of a new type of submarines rather slim, not many NATO submarines being suited for this type of operations. A German Type 201 submarine was lent to the Royal Norwegian Navy for evaluation and adaptation. The result was the Type 207, of which 15 vessels were delivered to Norway in the period 1964 – 67. All Kobben-class submarines were built by Rheinstahl Nordseewerke GmbH in Emden. Between 1985 – 93, six boats were lengthened by 2 m (6 ft 7 in) and modernized, most notably with new sonar equipment, including a towed passive array.

On 24 November 1972, the Kobben-class submarine HNoMS Sklinna of the Royal Norwegian Navy had "contact" with what they presumed was a Whiskey-class submarine, after 14 days of "hunt" in Sognefjord. Military documents released in 2009 confirm this episode.

During that period, four others were sold to the Royal Danish Navy (known there as the Tumleren class), three operational (modernized) and one for spare parts. HDMS Sælen served in the 2003 invasion of Iraq from May 2002 until June 2003.

In 2001, the Kobben class was completely phased out in Norwegian service, replaced by the newer Ula class. Five modernized vessels were given to the Polish Navy, four as operational units and one for spare parts. Before they were transferred, the Polish crews were trained and the boats were overhauled. 


Class overview

Builders

Nordseewerke GmbH

Operators	

     Royal Norwegian Navy
     Royal Danish Navy
     Polish Navy

Succeeded by	Ula class
Subclasses	Tumleren class
Built	1963–1966
In commission	1964–2021
Completed	15
General characteristics
Type	Coastal submarine
Displacement	

    435 t (428 long tons) surfaced
    485 t (477 long tons) submerged

Length	47.2 m (154 ft 10 in)
Beam	4.7 m (15 ft 5 in)
Draft	3.8 m (12 ft 6 in)
Propulsion	

    Diesel-electric
    2 MTU 1,100 hp (820 kW) diesel engines
    1 1,700 hp (1,300 kW) electric motor

Speed	

    10 knots (19 km/h; 12 mph) surfaced
    17 knots (31 km/h; 20 mph) submerged

Range	

    4,200 nmi (7,800 km; 4,800 mi) at 8 knots (15 km/h; 9.2 mph)
    228 nmi (422 km) at 4 knots (7.4 km/h; 4.6 mph)

Test depth	180 m (590 ft)
Crew Complement	24
Armament	

    8 533 mm (21.0 in) torpedo tubes capable of carrying the following torpedo types:
    T1, Mk-37 Mod 1/2, Tp 61, Tp 612, Tp 613


Sensors	Kelvin Hughes 1007 radarsystem, sonar Atlas Elektronik or Simrad
Fire control	Kongsberg MSI-70U
command control	Kongsberg MSI-70U
